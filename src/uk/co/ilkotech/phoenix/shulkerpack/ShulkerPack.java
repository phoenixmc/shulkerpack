package uk.co.ilkotech.phoenix.shulkerpack;

import net.minecraft.server.v1_11_R1.Item;
import net.minecraft.server.v1_11_R1.NBTTagCompound;
import net.minecraft.server.v1_11_R1.NBTTagList;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_11_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class ShulkerPack extends JavaPlugin implements Listener {

    // Inventory Tracking
    private HashMap<Player,Inventory> openInventory = new HashMap<Player, Inventory>();
    private HashMap<Inventory,ItemStack> shulkerInventory = new HashMap<Inventory,ItemStack>();


    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this,this);
    }

    // Stop players from dropping the currently open Shulker Box
    @EventHandler
    public void dropItem(PlayerDropItemEvent ev) {
        Inventory topInv = ev.getPlayer().getOpenInventory().getTopInventory();
        if (shulkerInventory.containsKey(topInv)) {
            ItemStack correctStack = shulkerInventory.get(topInv);
            if (correctStack.equals(ev.getItemDrop().getItemStack())) {
                ItemStack newBox = closeShulkerBox(ev.getPlayer(),correctStack,topInv);
                ev.getPlayer().closeInventory();
                ev.getItemDrop().setItemStack(newBox);
            }
        }
    }

    @EventHandler
    public void inventoryInteract(InventoryClickEvent ev) {
        Player player = (Player) ev.getWhoClicked();
        if (!player.getGameMode().equals(GameMode.ADVENTURE) && !player.getGameMode().equals(GameMode.SURVIVAL)) return;

        if (ev.getAction().equals(InventoryAction.PLACE_ALL) || ev.getAction().equals(InventoryAction.PLACE_ONE) || ev.getAction().equals(InventoryAction.PLACE_SOME) || ev.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
            ItemStack clickedItem = ev.getCursor();
            if (ev.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) clickedItem = ev.getCurrentItem();
            if (isShulkerBox(clickedItem)) {
                Inventory clickedInventory;
                if (!ev.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
                    clickedInventory = ev.getClickedInventory();
                } else {
                    // If we're moving to the other inventory get destination.
                    clickedInventory = player.getOpenInventory().getTopInventory();
                    if (ev.getClickedInventory() == clickedInventory) {
                        clickedInventory = player.getOpenInventory().getBottomInventory();
                    }
                }
                if (openInventory.containsKey(player)) {
                    if (openInventory.get(player).equals(clickedInventory)) {
                        ev.setCancelled(true);
                    }
                    return;
                }
            }
        }

        if (!ev.getClick().equals(ClickType.MIDDLE)) return;
        ItemStack clickedItem = ev.getCurrentItem();
        if (!isShulkerBox(clickedItem)) return;

        // We're happy it's a shulker box. But are we allowed?
        Inventory topInv = player.getOpenInventory().getTopInventory();
        if (topInv.getType() != InventoryType.CRAFTING && topInv.getType() != InventoryType.ENDER_CHEST) {
            player.sendMessage(ChatColor.YELLOW+"You can't open a shulker box in a shared inventory!");
            return;
        }
        openShulkerBox(player,clickedItem);
    }

    private void openShulkerBox(Player player, ItemStack shulkerBox) {
        if (shulkerBox.getType().equals(Material.AIR)) return;
        net.minecraft.server.v1_11_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(shulkerBox);

        NBTTagCompound tags = nmsStack.getTag();

        // Prepare to populate actual items
        Inventory inv = getServer().createInventory(player,27,"Shulker Box");
        if (tags != null) {
            NBTTagCompound blockEntityTag = tags.getCompound("BlockEntityTag");
            NBTTagList items = blockEntityTag.getList("Items", 10);
            for (int i = 0; i < items.size(); i++) {
                NBTTagCompound iStack = items.get(i);
                int slot = iStack.getInt("Slot");
                net.minecraft.server.v1_11_R1.ItemStack test = new net.minecraft.server.v1_11_R1.ItemStack(Item.b(iStack.getString("id")));
                test.setCount(iStack.getByte("Count"));
                test.setData(iStack.getShort("Damage"));
                test.setTag(iStack.getCompound("tag"));

                ItemStack bukkitStack = CraftItemStack.asBukkitCopy(test);
                inv.setItem(slot, bukkitStack);
            }
        }
        openInventory.put(player,inv);
        shulkerInventory.put(inv,shulkerBox.clone());
        player.openInventory(inv);
    }

    private boolean isShulkerBox(ItemStack item) {
        if (item == null) return false;
        if (item.getType().name() == null) return false;
        String[] name = item.getType().name().split("_");
        if (name.length >= 3) {
            if (name[name.length-2].equalsIgnoreCase("SHULKER") && name[name.length-1].equalsIgnoreCase("BOX")) return true;
        }
        return false;
    }

    @EventHandler
    public void inventoryClose(InventoryCloseEvent ev) {
        Player player = (Player) ev.getPlayer();
        Inventory inventory = ev.getInventory();

        if (!shulkerInventory.containsKey(inventory)) return;

        ItemStack shulkerBox = shulkerInventory.get(inventory);
        closeShulkerBox(player,shulkerBox,inventory);
    }

    /*
    * Closes a shulker box.
    * @player - Who owns the shulkerbox
    * @shulkerBox - The shulker box to close
    * @inventory - The current contents of the Shulker Box
    * */
    public ItemStack closeShulkerBox(Player player, ItemStack shulkerBox, Inventory inventory) {
        net.minecraft.server.v1_11_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(shulkerBox);

        // Create the new NBT mebe?
        NBTTagCompound tags = nmsStack.getTag();

        if (tags == null) {
            tags = new NBTTagCompound();
        }
        NBTTagCompound blockEntityTag = tags.getCompound("BlockEntityTag");
        if (blockEntityTag == null) {
            blockEntityTag = new NBTTagCompound();
        }
        NBTTagList items = new NBTTagList();

        int slot = 0;
        for (ItemStack i : inventory.getContents()) {
            if (i != null && !i.getType().equals(Material.AIR)) {
                net.minecraft.server.v1_11_R1.ItemStack newStack = CraftItemStack.asNMSCopy(i);
                NBTTagCompound itemNBT = newStack.save(new NBTTagCompound());
                itemNBT.setByte("Slot",(byte) slot);
                items.add(itemNBT);
            }
            slot++;
        }
        blockEntityTag.set("Items",items);
        tags.set("BlockEntityTag",blockEntityTag);
        nmsStack.setTag(tags);

        ItemStack newShulkerBox = CraftItemStack.asBukkitCopy(nmsStack);

        // Search for original?
        Inventory bottomInv = player.getOpenInventory().getBottomInventory();
        slot = 0;
        for (ItemStack i : bottomInv.getContents()) {
            if (i != null && i.equals(shulkerBox)) {
                bottomInv.setItem(slot,newShulkerBox);
                break;
            }
            slot++;
        }

        slot = 0;
        for (ItemStack i : player.getEnderChest().getContents()) {
            if (i != null && i.equals(shulkerBox)) {
                player.getEnderChest().setItem(slot,newShulkerBox);
                break;
            }
            slot++;
        }

        openInventory.remove(player);
        shulkerInventory.remove(inventory);
        return newShulkerBox;
    }
}
